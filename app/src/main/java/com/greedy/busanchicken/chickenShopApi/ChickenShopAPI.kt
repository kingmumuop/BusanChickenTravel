package com.greedy.busanchicken.chickenShopApi


import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface ChickenShopAPI {

    @GET("/api/15055133/v1/uddi:81f35d87-9127-49ee-aceb-8f3de07a774f")
    fun getChickenShopList(
        @Query("page") page: Int,
        @Query("perPage") perPage: Int,
        @Query("serviceKey") serviceKey: String
    ): Call<ChickenShopResponse>

}