package com.greedy.busanchicken.chickenShopApi

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
    var baseUrl: String = "https://api.odcloud.kr"
    var api: ChickenShopAPI

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(ChickenShopAPI::class.java)
    }
}