package com.greedy.busanchicken.chickenShopApi

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ChickenShop(
    @SerializedName("업소명")
    var shopName: String?,
    @SerializedName("소재지")
    var shopAddress: String?,
    @SerializedName("위도")
    var latitude: String?,
    @SerializedName("경도")
    var longitude: String?
) : Serializable {}
