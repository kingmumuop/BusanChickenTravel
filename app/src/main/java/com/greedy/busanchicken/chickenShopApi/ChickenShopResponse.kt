package com.greedy.busanchicken.chickenShopApi

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ChickenShopResponse(
    @SerializedName("currentCount")
    var currentCount: Int,
    @SerializedName("data")
    var dataResult: List<ChickenShop> = listOf()
) : Serializable
