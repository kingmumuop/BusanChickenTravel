package com.greedy.busanchicken


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast


import androidx.recyclerview.widget.RecyclerView
import com.greedy.busanchicken.chickenShopApi.ChickenShop

class ChickenShopAdapter(private val dataList: List<ChickenShop>) : RecyclerView.Adapter<ChickenShopAdapter.ChickenShopItemViewHolder>() {

    /* 1. */
    /* 목록에 포함 될 전체 데이터 항목의 갯수를 반환한다. */
    override fun getItemCount(): Int {
        return dataList.size
    }

    /* 2. */
    /* 항목을 보여줄 레이아웃(개별 뷰용 XML파일)을 반환한다. */
    override fun getItemViewType(position: Int) = R.layout.chicken_shop_list_item

    /* 3. */
    /* 2번에서 반환한 XML 파일을 inflate를 통해 객체로 만들어(메모리에 인지) 내부 클래스인 ViewHolder에게 넘김 */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChickenShopItemViewHolder {

        /*
            inflate 함수의 세가지 인자
            첫번째(viewType): 레이아웃 리소스의 식별자(2번에서 방금 만든 개별 뷰 xml 파일의 번호)
            두번째(parent): inflate 메소드의 실행 결과로 생성 될 뷰가 부착될 부모 뷰(뷰그룹, RecyclerView)
            세번째(false): 코드를 실행하는 시점에 부모 뷰에 뷰 객체를 부착해야 하는지 여부
                          (RecyclerView의 경우 뷰 내부에서 자체적으로 뷰의 부착을 진행하므로 반드시 false로 주고
                           parameter로만 던져 주는 개념으로 정의해야 한다.)
         */
        var view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)

        return ChickenShopItemViewHolder(view)
    }

    /* 4. */
    /* 뷰 홀더 */
    /*
        목록에 표시할 각 항목을 보여줄 뷰 객체와 뷰의 구성 요소를 제공받기 위해 사용할 뷰 홀더 클래스를 내부 클래스로 정의한다.
        개별 뷰 객체를 생성자로 전달하도록 주 생성자를 정의한다.(3번의 반환값이 전달됨)
        (개별 뷰의 구성 요소를 파악하고 데이터의 내용을 작성하는 것에 대한 정의라고 생각하자)
     */
    class ChickenShopItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        init {
            itemView.setOnClickListener {
                val pos = adapterPosition
                if (pos != RecyclerView.NO_POSITION) {
                    Toast.makeText(it.context,"$pos 클릭클릭",Toast.LENGTH_SHORT).show()
                    var bundle = Bundle()

                    bundle.putString("shopName", chickenShop.shopName)
                    bundle.putString("shopAddress", chickenShop.shopAddress)
                    bundle.putString("longitude", chickenShop.latitude)
                    bundle.putString("latitude", chickenShop.longitude)

                    val intent: Intent = Intent(it.context, ChickenShopDetail::class.java)
                    intent.putExtras(bundle)
                    Log.d("ggggg",bundle.getString("shopName").toString())

                    it.context.startActivity(intent)

                }
            }
        }

        lateinit var chickenShop: ChickenShop

//        val title = view.findViewById<TextView>(R.id.movie_title)
//        val hours = view.findViewById<TextView>(R.id.movie_hour)

        val shopName = view.findViewById<TextView>(R.id.shop_name)
        val shopAddress = view.findViewById<TextView>(R.id.shop_address)

        /* 데이터가 넘어오면 개별뷰의 해당 요소에 적용되도록 bind 함수를 정의 */
        fun bind(c: ChickenShop) {
            this.chickenShop = c

//            title.text = movie.title
//            hours.text = movie.hours

            shopName.text = chickenShop.shopName
            shopAddress.text = chickenShop.shopAddress


        }

    }

    /* 5. */
    /* 4번에서 정의 된 bind 함수를 활용해서 개별 항목 데이터를 개별 뷰에 작성, 스크롤을 내릴 때 자동 호출  */
    override fun onBindViewHolder(holder: ChickenShopItemViewHolder, position: Int) {

        holder.bind(dataList[position])
    }
}