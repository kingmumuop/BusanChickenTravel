package com.greedy.busanchicken

import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.greedy.busanchicken.chickenShopApi.ChickenShop
import com.greedy.busanchicken.chickenShopApi.ChickenShopResponse
import com.greedy.busanchicken.chickenShopApi.RetrofitBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class MainActivity : AppCompatActivity() {

    companion object{
        const val KEY = "2CmXQCmvHO2K%2B4NagsJlrJ5K0aafIVouPfZuuNOC5jZgRx9mB5thF9HmDXcxTf3lX416mG2KLnbpgSTryPNKEw%3D%3D"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // 화면 넘기는 버튼 객체 받아서 이벤트 추가
        val btn = findViewById<Button>(R.id.btn1)
        btn.setOnClickListener {

            showChickenShopList()
        }


    }

    fun showChickenShopList() {

        RetrofitBuilder.api
            .getChickenShopList(1,100,"2CmXQCmvHO2K+4NagsJlrJ5K0aafIVouPfZuuNOC5jZgRx9mB5thF9HmDXcxTf3lX416mG2KLnbpgSTryPNKEw==")
            .enqueue(object : Callback<ChickenShopResponse>{

                // Rest 방식의 비동기 통신 성공시
                override fun onResponse(
                    call: Call<ChickenShopResponse>,
                    response: Response<ChickenShopResponse>
                ) {
                    Log.d("ggggg","통신 성공했나?")

                    val chickenShopResponse = response.body()
                    Log.d("ggggg","치킨숍 리스폰스 결과 : " + response.message())

                    val chickenShopList: List<ChickenShop> = chickenShopResponse!!.dataResult
                    Log.d("ggggg","치킨숍 리스트에 제대로 담겼나? : " + chickenShopList.toString())

                    val bundle = Bundle()
                    bundle.putString("isthis", "이거 담겨 가나?")        // 재료 1 담기
                    bundle.putSerializable(
                        "chickenShopList",
                        (chickenShopList as java.io.Serializable)
                    )

                    val intent: Intent = Intent(this@MainActivity, ChickenShopListActivity::class.java)
                    intent.putExtras(bundle)

                    startActivity(intent)


                }

                override fun onFailure(call: Call<ChickenShopResponse>, t: Throwable) {
                    Toast.makeText(this@MainActivity, "${t.message}", Toast.LENGTH_SHORT).show()
                }


            })

    }
}














