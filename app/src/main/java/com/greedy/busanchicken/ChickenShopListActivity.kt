package com.greedy.busanchicken

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.greedy.busanchicken.chickenShopApi.ChickenShop

class ChickenShopListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chicken_shop_list_activity)

        val bundle = intent.extras
        Log.d("ggggg", bundle!!.getString("isthis").toString())

        val chickenShopList: List<ChickenShop> =
            bundle!!.getSerializable("chickenShopList") as List<ChickenShop>
        Log.d("ggggg", chickenShopList.toString())

    val layoutManager = LinearLayoutManager(this)

    val adapter = ChickenShopAdapter(chickenShopList)

        /* recyclerView를 활용하기 위한 재료 활용 */
        val recyclerView = findViewById<RecyclerView>(R.id.chicken_shop_list)
        recyclerView.setHasFixedSize(false)             // 부착 될 뷰들의 높이가 다를 수 있으면 false를 준다.(높이가 같으면 true를 전달해서 최적화를 유도할 수 있다.)
        recyclerView.layoutManager = layoutManager      // RecyclerView에 필요한 레이아웃 매니저 객체 설정
        recyclerView.adapter = adapter                  // RecyclerView에 필요한 어댑터 객체 설정




    }
}