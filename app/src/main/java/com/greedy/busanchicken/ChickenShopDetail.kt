package com.greedy.busanchicken

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.naver.maps.geometry.LatLng
import com.naver.maps.map.CameraUpdate
import com.naver.maps.map.MapFragment
import com.naver.maps.map.NaverMap
import com.naver.maps.map.OnMapReadyCallback
import com.naver.maps.map.overlay.Marker


class ChickenShopDetail : AppCompatActivity(), OnMapReadyCallback {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chicken_shop_detail)

        var shopName = findViewById<TextView>(R.id.shop_name)
        var shopAddress = findViewById<TextView>(R.id.shop_address)

        var bundle = intent.extras
        shopName.text = bundle!!.getString("shopName")
        shopAddress.text = bundle!!.getString("shopAddress")

        val fm = supportFragmentManager
        val mapFragment = fm.findFragmentById(R.id.map) as MapFragment?
            ?: MapFragment.newInstance().also {
                fm.beginTransaction().add(R.id.map, it).commit()
            }

        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(naverMap: NaverMap) {
        var bundle = intent.extras
        var latitude = bundle!!.getString("latitude")!!.toDouble()
        var longitude = bundle!!.getString("longitude")!!.toDouble()


        Log.d("ggggg","위도 : $latitude 경도 : $longitude")
        var coord = LatLng(latitude, longitude)
        val marker = Marker()
        marker.position = coord
        marker.map = naverMap

        Log.d("ggggg", "cood 출력 : ${coord.toString()}")

        val cameraPosition = CameraUpdate.scrollTo(coord)
        naverMap.moveCamera(cameraPosition)
        val cameraZoom = CameraUpdate.zoomTo(18.0)
        naverMap.moveCamera(cameraZoom)

        var ddd = naverMap.contentRegion[0]
        var ddf = naverMap.contentRegion[1]
        Log.d("ggggg",ddd.toString())
        Log.d("ggggg",ddf.toString())
    }


}